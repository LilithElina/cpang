setwd("/data3/genome_graphs/CPANG")
options(stringsAsFactors=FALSE)

library(dplyr)

# load pack data
packs <- read.table("playground/vg/CH3797_R1.vs.FivePsae.tsv", header=TRUE, sep="\t")

# are there nodes with no coverage?
packs[packs$coverage==0,]

# get mean coverage and zero count per node, and node length (n)
node_cov <- packs %>% 
  group_by(node.id) %>%
  summarise(cov_mean=mean(coverage),
            zero=sum(coverage==0),
            n=n())

# nodes without coverage
sum(node_cov$cov_mean==0)
node_cov %>%
  filter(cov_mean==0) %>%
  summarise(sum(n))

# nodes with part coverage
node_cov %>%
  filter(zero>=1, cov_mean>0) %>%
  group_by(zero) %>%
  tally() %>%
  arrange(desc(zero))

# nodes with one zero
node_cov %>%
  filter(zero==1, cov_mean>0) %>%
  group_by(n) %>%
  tally()
