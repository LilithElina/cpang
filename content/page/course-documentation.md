---
title: Course Documentation
author: LilithElina
date: '2020-03-17'
slug: course-documentation
categories: []
tags: []
comments: no
menu:
  - main
  - sidebar
weight: -250
---

## Target audience
This course is oriented towards biologists and bioinformaticians with at least an intermediate level of experience working with sequencing data formats and methods in the unix shell. The course will be of particular interest to researchers investigating organisms without a reference genome or populations featuring high levels of genetic diversity.

## Presentations and practicals

All the datasets used for this training course are available in the documentation.

### Day 1
[Intro slides](/assets/day1-intro.pdf) [PDF Download]  
[Slides](https://docs.google.com/presentation/d/1Iy0RaKseVhgmoKT9Hrzdh3sb2IWOF6GfmVm-y4di0dw/edit?usp=sharing) [Google drive]  
Practical 1: [toy examples]({{< ref "/toy_examples" >}})

### Day 2
[Slides](https://docs.google.com/presentation/d/1vClnCkGPZwqpVBZRbc4WQErlKNWCOWGaxRLmyuOZas0/edit?usp=sharing) [Google drive]  
Practical 2: [HIV]({{< ref "/HIV_exercises" >}})

### Day 3
[Slides](https://docs.google.com/presentation/d/1RrfXMI7mpRtYu-H5OswPL2LZcIN_OC0NuI_m6mwU38U/edit?usp=sharing) [Google drive]  
Practical 3: [Bacteria]({{< ref "/bacteria" >}})

### Day 4
[Slides](https://docs.google.com/presentation/d/1xO40HtzgT-IFkM_93GWLi8wxOw4uADoH4rnuXQ8pu9Y/edit?usp=sharing) [Google drive]  
Practical 4: [MHC]({{< ref "/mhc" >}})

### Day 5
[Slides](https://docs.google.com/presentation/d/1Yabcw_M5gCcCv9DJQ4d3Fp0X-GzooP-y08Qp0Fb7Iuc/edit?usp=sharing) [Google drive]  
[related slides](https://docs.google.com/presentation/d/10KDUnRJyKwi5q9khCcb0C9Mb52XcGZbfvFFF0nNCbGg/edit?usp=sharing) [Google drive]  
Practical 5: [MHC (continued)]({{< ref "/mhc" >}})


---


### [Learning objectives]({{< ref "learning-objectives" >}})

### [Instructors]({{< ref "instructors" >}})