---
description: About this website
menu: main
title: About
weight: -100
---

The source for this webpage is [on gitlab](https://gitlab.com/LilithElina/cpang), forked from this [original repo](https://github.com/GTPB/CPANG19) and then migrated to [blogdown](https://github.com/rstudio/blogdown).

The Hugo theme used here is minimo:

- You can find the source code for minimo at [GitHub](https://github.com/MunifTanjim/minimo)

- You can find the source code for Hugo at [GitHub](https://github.com/gohugoio/hugo)


**You can find out more about the author of this webpage in her [pagedown](https://github.com/rstudio/pagedown)-rendered [CV](https://sarah-pohl-cv.netlify.com/).**