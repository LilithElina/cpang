---
title: Installing and Updating Other Tools
date: '2020-03-20'
lastmod: '2020-04-20'
slug: other-tools
categories:
  - installation
tags: []
authors: [lilithelina]
toc: true
---

I previously installed other tools like [minimap2](https://github.com/lh3/minimap2), [seqwish](https://github.com/ekg/seqwish), and [odgi](https://github.com/vgteam/odgi) when I needed them (in these cases in the [day 2 post]({{< ref "2019-10-14-day-2-hiv-exercises" >}}), and for `vcffilter` and [Sambamba](http://lomereiter.github.io/sambamba/) in the [day 1 post]({{< ref "2019-10-07-day-1-toy-examples#exploring-the-benefits-of-graphs-for-read-mapping" >}})). If there are more tools to install or update, this could get messy, so I've started this post to collect all the (new) installation and update protocols.

## odgi

[odgi is now available via bioconda](https://anaconda.org/bioconda/odgi), so I'm going to remove the version I installed and install it via conda instead, which should be easier to maintain.

```bash
cd /data3/genome_graphs/
sudo rm -r odgi
sudo -i
conda install -c bioconda odgi
```

```
Collecting package metadata (current_repodata.json): done
Solving environment: failed with initial frozen solve. Retrying with flexible solve.
Solving environment: failed with repodata from current_repodata.json, will retry with next repodata source.
Collecting package metadata (repodata.json): done
Solving environment: failed with initial frozen solve. Retrying with flexible solve.
Solving environment: |
Found conflicts! Looking for incompatible packages.                                                              failed                                                                      -                                      \

UnsatisfiableError: The following specifications were found
to be incompatible with the existing python installation in your environment:

Specifications:

  - odgi -> python[version='>=3.6,<3.7.0a0|>=3.7,<3.8.0a0']

Your python: python=2.7

If python is on the left-most side of the chain, that's the version you've asked for.
When python appears to the right, that indicates that the thing on the left is somehow
not available for the python version you are constrained to. Note that conda will not
change your python version to a different minor version unless you explicitly specify
that.
```

Ah, python is the problem!

```bash
conda activate python3.7
conda install -c bioconda odgi
```

```
Collecting package metadata (current_repodata.json): done
Solving environment: done


==> WARNING: A newer version of conda exists. <==
  current version: 4.8.2
  latest version: 4.8.3

Please update conda by running

    $ conda update -n base conda



## Package Plan ##

  environment location: /usr/bin/miniconda3/envs/python3.7

  added / updated specs:
    - odgi


The following packages will be downloaded:

    package                    |            build
    ---------------------------|-----------------
    certifi-2019.11.28         |   py37hc8dfbb8_1         149 KB  conda-forge
    odgi-0.2                   |   py37h8b12597_0        58.9 MB  bioconda
    openssl-1.1.1e             |       h516909a_0         2.1 MB  conda-forge
    python_abi-3.7             |          1_cp37m           4 KB  conda-forge
    ------------------------------------------------------------
                                           Total:        61.2 MB

The following NEW packages will be INSTALLED:

  odgi               bioconda/linux-64::odgi-0.2-py37h8b12597_0
  python_abi         conda-forge/linux-64::python_abi-3.7-1_cp37m

The following packages will be UPDATED:

  certifi                                 2019.11.28-py37_0 --> 2019.11.28-py37hc8dfbb8_1
  openssl                                 1.1.1d-h516909a_0 --> 1.1.1e-h516909a_0


Proceed ([y]/n)? y


Downloading and Extracting Packages
certifi-2019.11.28   | 149 KB    | ######################################################################## | 100%
openssl-1.1.1e       | 2.1 MB    | ######################################################################## | 100%
python_abi-3.7       | 4 KB      | ######################################################################## | 100%
odgi-0.2             | 58.9 MB   | ######################################################################## | 100%
Preparing transaction: done
Verifying transaction: done
Executing transaction: done
```

### Testing odgi

Great, that was easy! Now let's see if I'm now dependent on that conda environment to use odgi.

```bash
exit
odgi
```

```
odgi: command not found
```

```bash
conda info -e
```

```
# conda environments:
#
base                  *  /usr/bin/miniconda3
circos_env               /usr/bin/miniconda3/envs/circos_env
ddocent_env              /usr/bin/miniconda3/envs/ddocent_env
pang                     /usr/bin/miniconda3/envs/pang
prokka_env               /usr/bin/miniconda3/envs/prokka_env
python3.7                /usr/bin/miniconda3/envs/python3.7
```

It doesn't work in the base environment.

```bash
conda activate python3.7
```

```
CommandNotFoundError: Your shell has not been properly configured to use 'conda activate'.
To initialize your shell, run

    $ conda init <SHELL_NAME>

Currently supported shells are:
  - bash
  - fish
  - tcsh
  - xonsh
  - zsh
  - powershell

See 'conda init --help' for more information and options.

IMPORTANT: You may need to close and restart your shell after running 'conda init'.

```

OK, I did not expect that, but so far I used conda only for installation purposes as root, never as normal user.

```bash
conda init bash
```

```
no change     /usr/bin/miniconda3/condabin/conda
no change     /usr/bin/miniconda3/bin/conda
no change     /usr/bin/miniconda3/bin/conda-env
no change     /usr/bin/miniconda3/bin/activate
no change     /usr/bin/miniconda3/bin/deactivate
no change     /usr/bin/miniconda3/etc/profile.d/conda.sh
no change     /usr/bin/miniconda3/etc/fish/conf.d/conda.fish
no change     /usr/bin/miniconda3/shell/condabin/Conda.psm1
no change     /usr/bin/miniconda3/shell/condabin/conda-hook.ps1
no change     /usr/bin/miniconda3/lib/python2.7/site-packages/xontrib/conda.xsh
no change     /usr/bin/miniconda3/etc/profile.d/conda.csh
modified      /home/spo12/.bashrc

==> For changes to take effect, close and re-open your current shell. <==
```

Closing and re-opening a connection to the server... Oooh, I now have "(base)" in front of my name.

```bash
cd /data3/genome_graphs/
conda activate python3.7
odgi
```

```
odgi: dynamic succinct variation graph tool

usage: odgi <command> [options]

main mapping and calling pipeline:
  -- build         build dynamic succinct variation graph
  -- stats         extract statistics and properties of the graph
  -- sort          topologically order the graph
  -- view          projection of graphs into other formats
  -- kmers         process and dump the kmers of the graph
  -- unitig        emit the unitigs of the graph
  -- viz           visualize the graph
  -- paths         interrogation and manipulation of paths
  -- prune         prune the graph based on coverage or topological complexity
  -- simplify      merge unbranching linear components into single nodes
  -- subset        merge unbranching linear components into single nodes
  -- bin           bin path information across the graph
  -- matrix        graph topology in sparse matrix form
  -- chop          chop long nodes into short ones while preserving topology
  -- layout        use SGD to make 2D layouts of the graph
  -- flatten       project the graph sequence and paths into FASTA and BED
  -- break         break cycles in the graph
  -- test          run unit tests

For more commands, type `odgi help`.
```

To see if this works as before, I'll retrace some steps from [day 3]({{< ref "2019-11-13-day-3-bacterial-pangenomics" >}}).

```bash
cd CPANG/playground/day3/references/
mkdir new_odgi
gunzip FivePsae.gfa.gz
cd new_odgi
odgi build -g ../FivePsae.gfa -o - | odgi sort -i - -o FivePsae.og
odgi viz -i FivePsae.og -o FivePsae.png -x 2000 -R
```

Interesting. The produced figure is still the same, but the output about the paths in the graph is missing from this version of odgi.

![pangenome graph based on five references](/day3/pics/FivePsae_new.png)


## Sequence Tube Maps (IVG)

So far, I've only used [IVG](https://vgteam.github.io/sequenceTubeMap/) online, but it is also possible to [install it locally](https://github.com/vgteam/sequenceTubeMap) to visualise bigger graphs.

For this we need either npm or yarn, which we both don't have on our server. I'll go with npm.

```bash
sudo apt install npm
```

```
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following additional packages will be installed:
  gyp libjs-inherits libjs-node-uuid libuv1-dev node-abbrev node-ansi node-ansi-color-table node-archy node-async
  node-block-stream node-combined-stream node-cookie-jar node-delayed-stream node-forever-agent node-form-data
  node-fstream node-fstream-ignore node-github-url-from-git node-glob node-graceful-fs node-gyp node-inherits
  node-ini node-json-stringify-safe node-lockfile node-lru-cache node-mime node-minimatch node-mkdirp
  node-mute-stream node-node-uuid node-nopt node-normalize-package-data node-npmlog node-once node-osenv node-qs
  node-read node-read-package-json node-request node-retry node-rimraf node-semver node-sha node-sigmund node-slide
  node-tar node-tunnel-agent node-underscore node-which nodejs-dev
Suggested packages:
  node-hawk node-aws-sign node-oauth-sign node-http-signature
The following NEW packages will be installed:
  gyp libjs-inherits libjs-node-uuid libuv1-dev node-abbrev node-ansi node-ansi-color-table node-archy node-async
  node-block-stream node-combined-stream node-cookie-jar node-delayed-stream node-forever-agent node-form-data
  node-fstream node-fstream-ignore node-github-url-from-git node-glob node-graceful-fs node-gyp node-inherits
  node-ini node-json-stringify-safe node-lockfile node-lru-cache node-mime node-minimatch node-mkdirp
  node-mute-stream node-node-uuid node-nopt node-normalize-package-data node-npmlog node-once node-osenv node-qs
  node-read node-read-package-json node-request node-retry node-rimraf node-semver node-sha node-sigmund node-slide
  node-tar node-tunnel-agent node-underscore node-which nodejs-dev npm
0 upgraded, 52 newly installed, 0 to remove and 0 not upgraded.
Need to get 2,543 kB of archives.
After this operation, 16.0 MB of additional disk space will be used.
```

This installs a lot of new packages, so I'm not copying the whole output here. Everything ran through fine, so this is expected to work.

```bash
cd /data3/genome_graphs/
git clone https://github.com/vgteam/sequenceTubeMap.git
```

```
Cloning into 'sequenceTubeMap'...
remote: Enumerating objects: 46, done.
remote: Counting objects: 100% (46/46), done.
remote: Compressing objects: 100% (41/41), done.
remote: Total 2003 (delta 19), reused 16 (delta 5), pack-reused 1957
Receiving objects: 100% (2003/2003), 32.85 MiB | 6.09 MiB/s, done.
Resolving deltas: 100% (1236/1236), done.
```

```
cd sequenceTubeMap/
npm install
```

```
loadDep:websocket → get   ▌ ╢██████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:websocket → mapTo ▄ ╢██████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:websocket → addNa ▌ ╢██████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine bootstrap@4.3.1: wanted: {"node":">=6"} (current: {"node":"4.2.6","npm":"3.5.2"})
WARN engine @fortawesome/fontawesome-svg-core@1.2.28: wanted: {"node":">=6"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:websocket → resol ▄ ╢██████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:@fortawesome/font ▌ ╢██████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine @fortawesome/fontawesome-common-types@0.2.28: wanted: {"node":">=6"} (current: {"node":"4.2.6","npm":"3.5.2loadDep:fs-extra → resolv ▌ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:fsevents → header ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine postcss-normalize@7.0.1: wanted: {"node":">=6.0.0"} (current: {"node":"4.2.6","npm":"3.5.2"})
WARN engine pnp-webpack-plugin@1.2.1: wanted: {"node":">=6"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:fsevents → addNam ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine postcss-safe-parser@4.0.1: wanted: {"node":">=6.0.0"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:fsevents → get    ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine url-loader@1.1.2: wanted: {"node":">= 6.9.0"} (current: {"node":"4.2.6","npm":"3.5.2"})
WARN engine dotenv@6.2.0: wanted: {"node":">=6"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:fsevents → mapToR ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:fsevents → get    ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:fsevents → header ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine @svgr/webpack@4.1.0: wanted: {"node":">=8"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:fsevents → mapToR ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine mini-css-extract-plugin@0.5.0: wanted: {"node":">= 6.9.0 <7.0.0 || >= 8.9.0"} (current: {"node":"4.2.6","nploadDep:fsevents → networ ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:fsevents → header ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:fsevents → resolv ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟)
loadDep:fsevents → mapToR ▌ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine babel-loader@8.0.5: wanted: {"node":">= 6.9"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:fsevents → addNam ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:fsevents → get    ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:fsevents → fetch  ▌ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine babel-eslint@10.0.1: wanted: {"node":">=6"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:fsevents → fetch  ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:fsevents → resolv ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:fsevents → addNam ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:fsevents → resolv ▌ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine webpack-dev-server@3.2.1: wanted: {"node":">= 6.11.5"} (current: {"node":"4.2.6","npm":"3.5.2"})
WARN engine react-dev-utils@9.1.0: wanted: {"node":">=8.10"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:fsevents → addTmp ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:fsevents → resolv ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine @typescript-eslint/parser@1.6.0: wanted: {"node":"^6.14.0 || ^8.10.0 || >=9.10.0"} (current: {"node":"4.2.6loadDep:fsevents → resolv ▌ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:fsevents → resolv ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine @typescript-eslint/eslint-plugin@1.6.0: wanted: {"node":"^6.14.0 || ^8.10.0 || >=9.10.0"} (current: {"node"loadDep:source-map → addR ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:source-map → reso ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:loader-utils → ad ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine @svgr/core@4.3.3: wanted: {"node":">=8"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:loader-utils → re ▌ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:semver → addNamed ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:leven → headers   ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:@svgr/plugin-jsx  ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:svg-parser → 304  ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine @svgr/hast-util-to-babel-ast@4.3.2: wanted: {"node":">=8"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:semver → afterAdd ▌ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:@svgr/babel-plugi ▌ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine @svgr/babel-plugin-add-jsx-attribute@4.2.0: wanted: {"node":">=8"} (current: {"node":"4.2.6","npm":"3.5.2"})
WARN engine @svgr/babel-plugin-replace-jsx-attribute-value@4.2.0: wanted: {"node":">=8"} (current: {"node":"4.2.6","npm":"3.5.2"})
WARN engine @svgr/babel-plugin-svg-em-dimensions@4.2.0: wanted: {"node":">=8"} (current: {"node":"4.2.6","npm":"3.5.2"})
WARN engine @svgr/babel-plugin-remove-jsx-empty-expression@4.2.0: wanted: {"node":">=8"} (current: {"node":"4.2.6","npm":"3.5.2"})
WARN engine @svgr/babel-plugin-svg-dynamic-title@4.3.3: wanted: {"node":">=8"} (current: {"node":"4.2.6","npm":"3.5.2"})
WARN engine @svgr/babel-plugin-transform-svg-component@4.2.0: wanted: {"node":">=8"} (current: {"node":"4.2.6","npm":"3.5.2"})
WARN engine @svgr/babel-plugin-transform-react-native-svg@4.2.0: wanted: {"node":">=8"} (current: {"node":"4.2.6","npm":"3.5.2"})
WARN engine @svgr/babel-plugin-remove-jsx-attribute@4.2.0: wanted: {"node":">=8"} (current: {"node":"4.2.6","npm":"3.5.loadDep:util.promisify →  ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine css-tree@1.0.0-alpha.37: wanted: {"node":">=8.0.0"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:nth-check → 200   ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:css-tree → resolv ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:tsutils → 200     ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:tsutils → resolve ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine @typescript-eslint/typescript-estree@1.6.0: wanted: {"node":">=6.14.0"} (current: {"node":"4.2.6","npm":"3.loadDep:slash → request   ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:slash → addNamed  ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine babel-plugin-istanbul@5.2.0: wanted: {"node":">=6"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:slash → cache add ▌ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:slash → resolveWi ▌ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:write-file-atomic ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine pirates@4.0.1: wanted: {"node":">= 6"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:write-file-atomic ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine jest-haste-map@24.9.0: wanted: {"node":">= 6"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:find-up           ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:test-exclude → re ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:test-exclude → ad ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:test-exclude → fe ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:locate-path → fet ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:p-locate → fetch  ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:p-try → resolveWi ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:istanbul-lib-cove ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:require-main-file ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:require-main-file ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:locate-path → res ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine locate-path@3.0.0: wanted: {"node":">=6"} (current: {"node":"4.2.6","npm":"3.5.2"})
WARN engine p-locate@3.0.0: wanted: {"node":">=6"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:p-limit → cache   ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:fsevents → cache  ▌ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:fsevents → retry  ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:fsevents → addRem ▌ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:urix → 200        ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
npm WARN deprecated resolve-url@0.2.1: https://github.com/lydell/resolve-url#deprecated
npm WARN deprecated urix@0.1.0: Please see https://github.com/lydell/urix#deprecated
loadDep:jest-util         ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:source-map → get  ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine @jest/source-map@24.9.0: wanted: {"node":">= 6"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:source-map → reso ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine @jest/fake-timers@24.9.0: wanted: {"node":">= 6"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:source-map → afte ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:jest-mock → resol ▌ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine jest-message-util@24.9.0: wanted: {"node":">= 6"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:ci-info → fetch   ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:execa → request   ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:execa → mapToRegi ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:rsvp → 200        ▌ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:strip-eof → 200   ▌ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine get-stream@4.1.0: wanted: {"node":">=6"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:babel-plugin-jest ▌ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:find-cache-dir →  ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:pkg-dir → resolve ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine pkg-dir@3.0.0: wanted: {"node":">=6"} (current: {"node":"4.2.6","npm":"3.5.2"})
loadDep:pkg-dir → afterAd ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:semver → fetch    ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:locate-path → res ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:locate-path → req ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:p-locate → 304    ▄ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:p-limit → afterAd ▀ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:babel-preset-reac ▌ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:@babel/plugin-syn ▐ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
loadDep:cosmiconfig       ▌ ╢██████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
WARN engine import-fresh@3.2.1: wanted: {"node":">=6"} (current: {"node":"4.2.6","npm":"3.5.2"})
WARN engine parse-json@5.0.0: wanted: {"node":">=8"} (current: {"node":"4.2.6","npm":"3.5.2"})
npm ERR! Linux 4.4.0-174-generic
npm ERR! argv "/usr/bin/nodejs" "/usr/bin/npm" "install"
npm ERR! node v4.2.6
npm ERR! npm  v3.5.2
npm ERR! code EMISSINGARG

npm ERR! typeerror Error: Missing required argument #1
npm ERR! typeerror     at andLogAndFinish (/usr/share/npm/lib/fetch-package-metadata.js:31:3)
npm ERR! typeerror     at fetchPackageMetadata (/usr/share/npm/lib/fetch-package-metadata.js:51:22)
npm ERR! typeerror     at resolveWithNewModule (/usr/share/npm/lib/install/deps.js:456:12)
npm ERR! typeerror     at /usr/share/npm/lib/install/deps.js:457:7
npm ERR! typeerror     at /usr/share/npm/node_modules/iferr/index.js:13:50
npm ERR! typeerror     at /usr/share/npm/lib/fetch-package-metadata.js:37:12
npm ERR! typeerror     at addRequestedAndFinish (/usr/share/npm/lib/fetch-package-metadata.js:82:5)
npm ERR! typeerror     at returnAndAddMetadata (/usr/share/npm/lib/fetch-package-metadata.js:117:7)
npm ERR! typeerror     at pickVersionFromRegistryDocument (/usr/share/npm/lib/fetch-package-metadata.js:134:20)
npm ERR! typeerror     at /usr/share/npm/node_modules/iferr/index.js:13:50
npm ERR! typeerror This is an error with npm itself. Please report this error at:
npm ERR! typeerror     <http://github.com/npm/npm/issues>

npm ERR! Please include the following file with any support request:
npm ERR!     /data3/genome_graphs/sequenceTubeMap/npm-debug.log
```

### Updating node.js

Ugh, so installing npm installed a lot of packages, including node and related stuff, but with version 4.2.6 (and npm 3.5.2) and we need much newer version(s). Let's [install](https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-16-04) those.

```bash
cd ~
curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh
```

```
================================================================================
================================================================================

                              DEPRECATION WARNING

  Node.js 8.x LTS Carbon is no longer actively supported!

  You will not receive security or critical stability updates for this version.

  You should migrate to a supported version of Node.js as soon as possible.
  Use the installation script that corresponds to the version of Node.js you
  wish to install. e.g.

   * https://deb.nodesource.com/setup_10.x — Node.js 10 LTS "Dubnium" (recommended)
   * https://deb.nodesource.com/setup_12.x — Node.js 12 LTS "Erbium"

  Please see https://github.com/nodejs/Release for details about which
  version may be appropriate for you.

  The NodeSource Node.js distributions repository contains
  information both about supported versions of Node.js and supported Linux
  distributions. To learn more about usage, see the repository:
    https://github.com/nodesource/distributions

================================================================================
================================================================================
```

🙄 

```bash
curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh
```

```
## Installing the NodeSource Node.js 10.x repo...


## Populating apt-get cache...

+ apt-get update
Hit:1 http://archive.ubuntu.com/ubuntu xenial InRelease
Hit:2 http://archive.ubuntu.com/ubuntu xenial-updates InRelease
Hit:3 http://archive.ubuntu.com/ubuntu xenial-security InRelease
Hit:4 http://archive.ubuntu.com/ubuntu xenial-backports InRelease
Hit:5 https://deb.nodesource.com/node_8.x xenial InRelease
Ign:6 https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.2 InRelease
Hit:7 https://download.docker.com/linux/ubuntu xenial InRelease
Hit:8 https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.2 Release
Reading package lists... Done

## Confirming "xenial" is supported...

+ curl -sLf -o /dev/null 'https://deb.nodesource.com/node_10.x/dists/xenial/Release'

## Adding the NodeSource signing key to your keyring...

+ curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -
OK

## Creating apt sources list file for the NodeSource Node.js 10.x repo...

+ echo 'deb https://deb.nodesource.com/node_10.x xenial main' > /etc/apt/sources.list.d/nodesource.list
+ echo 'deb-src https://deb.nodesource.com/node_10.x xenial main' >> /etc/apt/sources.list.d/nodesource.list

## Running `apt-get update` for you...

+ apt-get update
Hit:1 http://archive.ubuntu.com/ubuntu xenial InRelease
Hit:2 http://archive.ubuntu.com/ubuntu xenial-updates InRelease
Hit:3 http://archive.ubuntu.com/ubuntu xenial-security InRelease
Hit:4 http://archive.ubuntu.com/ubuntu xenial-backports InRelease
Hit:5 https://download.docker.com/linux/ubuntu xenial InRelease
Get:6 https://deb.nodesource.com/node_10.x xenial InRelease [4,584 B]
Ign:7 https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.2 InRelease
Hit:8 https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.2 Release
Get:9 https://deb.nodesource.com/node_10.x xenial/main amd64 Packages [767 B]
Fetched 5,351 B in 0s (5,630 B/s)
Reading package lists... Done

## Run `sudo apt-get install -y nodejs` to install Node.js 10.x and npm
## You may also need development tools to build native addons:
     sudo apt-get install gcc g++ make
## To install the Yarn package manager, run:
     curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
     echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
     sudo apt-get update && sudo apt-get install yarn
```

That's better.

```bash
sudo apt-get install -y nodejs
```

```
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following packages were automatically installed and are no longer required:
  gyp libjs-inherits libjs-node-uuid libuv1 libuv1-dev node-abbrev node-ansi node-ansi-color-table node-archy
  node-async node-block-stream node-combined-stream node-cookie-jar node-delayed-stream node-forever-agent
  node-form-data node-fstream node-fstream-ignore node-github-url-from-git node-glob node-graceful-fs node-gyp
  node-inherits node-ini node-json-stringify-safe node-lockfile node-lru-cache node-mime node-minimatch node-mkdirp
  node-mute-stream node-node-uuid node-nopt node-normalize-package-data node-npmlog node-once node-osenv node-qs
  node-read node-read-package-json node-request node-retry node-rimraf node-semver node-sha node-sigmund node-slide
  node-tar node-tunnel-agent node-underscore node-which
Use 'sudo apt autoremove' to remove them.
The following packages will be REMOVED:
  nodejs-dev npm
The following packages will be upgraded:
  nodejs
1 upgraded, 0 newly installed, 2 to remove and 5 not upgraded.
Need to get 16.2 MB of archives.
After this operation, 56.7 MB of additional disk space will be used.
Get:1 https://deb.nodesource.com/node_10.x xenial/main amd64 nodejs amd64 10.20.1-1nodesource1 [16.2 MB]
Fetched 16.2 MB in 0s (17.0 MB/s)
(Reading database ... 328127 files and directories currently installed.)
Removing npm (3.5.2-0ubuntu4) ...
dpkg: nodejs-dev: dependency problems, but removing anyway as you requested:
 node-gyp depends on nodejs-dev.

Removing nodejs-dev (4.2.6~dfsg-1ubuntu4.2) ...
Processing triggers for man-db (2.7.5-1) ...
(Reading database ... 325070 files and directories currently installed.)
Preparing to unpack .../nodejs_10.20.1-1nodesource1_amd64.deb ...
Unpacking nodejs (10.20.1-1nodesource1) over (4.2.6~dfsg-1ubuntu4.2) ...
Processing triggers for man-db (2.7.5-1) ...
Setting up nodejs (10.20.1-1nodesource1) ...
```

### Installing packages for IVG

Back to the Tube Maps!

```
cd /data3/genome_graphs/sequenceTubeMap/
npm install
```

```
npm WARN deprecated fsevents@2.0.6: Please update: there are crash fixes
npm WARN deprecated chokidar@2.1.8: Chokidar 2 will break on node v14+. Upgrade to chokidar 3 with 15x less dependencies.
npm WARN deprecated popper.js@1.16.1: You can find the new Popper v2 at @popperjs/core, this package is dedicated to the legacy v1
npm WARN deprecated request@2.88.2: request has been deprecated, see https://github.com/request/request/issues/3142
npm WARN deprecated fsevents@1.2.12: fsevents 1 will break on node v14+. Upgrade to fsevents 2 with massive improvements.
npm WARN deprecated resolve-url@0.2.1: https://github.com/lydell/resolve-url#deprecated
npm WARN deprecated urix@0.1.0: Please see https://github.com/lydell/urix#deprecated
npm WARN deprecated left-pad@1.3.0: use String.prototype.padStart()
npm WARN deprecated core-js@2.6.11: core-js@<3 is no longer maintained and not recommended for usage due to the number of issues. Please, upgrade your dependencies to the actual version of core-js@3.

> websocket@1.0.31 install /data3/genome_graphs/sequenceTubeMap/node_modules/websocket
> (node-gyp rebuild 2> builderror.log) || (exit 0)

make: Entering directory '/data3/genome_graphs/sequenceTubeMap/node_modules/websocket/build'
  CXX(target) Release/obj.target/bufferutil/src/bufferutil.o
  SOLINK_MODULE(target) Release/obj.target/bufferutil.node
  COPY Release/bufferutil.node
  CXX(target) Release/obj.target/validation/src/validation.o
  SOLINK_MODULE(target) Release/obj.target/validation.node
  COPY Release/validation.node
make: Leaving directory '/data3/genome_graphs/sequenceTubeMap/node_modules/websocket/build'

> core-js@2.6.11 postinstall /data3/genome_graphs/sequenceTubeMap/node_modules/babel-runtime/node_modules/core-js
> node -e "try{require('./postinstall')}catch(e){}"

Thank you for using core-js ( https://github.com/zloirock/core-js ) for polyfilling JavaScript standard library!

The project needs your help! Please consider supporting of core-js on Open Collective or Patreon:
> https://opencollective.com/core-js
> https://www.patreon.com/zloirock

Also, the author of core-js ( https://github.com/zloirock ) is looking for a good job -)


> core-js@3.6.5 postinstall /data3/genome_graphs/sequenceTubeMap/node_modules/core-js
> node -e "try{require('./postinstall')}catch(e){}"

npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@2.0.6 (node_modules/react-scripts/node_modules/fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@2.0.6: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@^1.2.7 (node_modules/jest-haste-map/node_modules/fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.12: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@^1.2.7 (node_modules/chokidar/node_modules/fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.12: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
npm WARN bootstrap@4.3.1 requires a peer of jquery@1.9.1 - 3 but none is installed. You must install peer dependencies yourself.
npm WARN @typescript-eslint/eslint-plugin@1.6.0 requires a peer of typescript@* but none is installed. You must install peer dependencies yourself.
npm WARN @typescript-eslint/parser@1.6.0 requires a peer of typescript@* but none is installed. You must install peer dependencies yourself.
npm WARN @typescript-eslint/typescript-estree@1.6.0 requires a peer of typescript@* but none is installed. You must install peer dependencies yourself.
npm WARN tsutils@3.17.1 requires a peer of typescript@>=2.8.0 || >= 3.2.0-dev || >= 3.3.0-dev || >= 3.4.0-dev || >= 3.5.0-dev || >= 3.6.0-dev || >= 3.6.0-beta || >= 3.7.0-dev || >= 3.7.0-beta but none is installed. You must install peer dependencies yourself.
npm WARN sequence-tube-maps@0.1.0 No repository field.
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: abbrev@1.1.1 (node_modules/chokidar/node_modules/fsevents/node_modules/abbrev):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/abbrev' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.abbrev.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: ansi-regex@2.1.1 (node_modules/chokidar/node_modules/fsevents/node_modules/ansi-regex):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/ansi-regex' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.ansi-regex.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: aproba@1.2.0 (node_modules/chokidar/node_modules/fsevents/node_modules/aproba):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/aproba' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.aproba.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: balanced-match@1.0.0 (node_modules/chokidar/node_modules/fsevents/node_modules/balanced-match):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/balanced-match' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.balanced-match.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: chownr@1.1.4 (node_modules/chokidar/node_modules/fsevents/node_modules/chownr):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/chownr' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.chownr.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: code-point-at@1.1.0 (node_modules/chokidar/node_modules/fsevents/node_modules/code-point-at):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/code-point-at' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.code-point-at.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: concat-map@0.0.1 (node_modules/chokidar/node_modules/fsevents/node_modules/concat-map):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/concat-map' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.concat-map.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: console-control-strings@1.1.0 (node_modules/chokidar/node_modules/fsevents/node_modules/console-control-strings):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/console-control-strings' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.console-control-strings.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: core-util-is@1.0.2 (node_modules/chokidar/node_modules/fsevents/node_modules/core-util-is):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/core-util-is' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.core-util-is.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: deep-extend@0.6.0 (node_modules/chokidar/node_modules/fsevents/node_modules/deep-extend):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/deep-extend' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.deep-extend.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: delegates@1.0.0 (node_modules/chokidar/node_modules/fsevents/node_modules/delegates):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/delegates' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.delegates.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: detect-libc@1.0.3 (node_modules/chokidar/node_modules/fsevents/node_modules/detect-libc):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/detect-libc' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.detect-libc.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fs.realpath@1.0.0 (node_modules/chokidar/node_modules/fsevents/node_modules/fs.realpath):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/fs.realpath' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.fs.realpath.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: has-unicode@2.0.1 (node_modules/chokidar/node_modules/fsevents/node_modules/has-unicode):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/has-unicode' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.has-unicode.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: inherits@2.0.4 (node_modules/chokidar/node_modules/fsevents/node_modules/inherits):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/inherits' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.inherits.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: ini@1.3.5 (node_modules/chokidar/node_modules/fsevents/node_modules/ini):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/ini' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.ini.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: isarray@1.0.0 (node_modules/chokidar/node_modules/fsevents/node_modules/isarray):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/isarray' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.isarray.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: minimist@1.2.5 (node_modules/chokidar/node_modules/fsevents/node_modules/minimist):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/minimist' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.minimist.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: ms@2.1.2 (node_modules/chokidar/node_modules/fsevents/node_modules/ms):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/ms' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.ms.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: npm-normalize-package-bin@1.0.1 (node_modules/chokidar/node_modules/fsevents/node_modules/npm-normalize-package-bin):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/npm-normalize-package-bin' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.npm-normalize-package-bin.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: number-is-nan@1.0.1 (node_modules/chokidar/node_modules/fsevents/node_modules/number-is-nan):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/number-is-nan' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.number-is-nan.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: object-assign@4.1.1 (node_modules/chokidar/node_modules/fsevents/node_modules/object-assign):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/object-assign' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.object-assign.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: os-homedir@1.0.2 (node_modules/chokidar/node_modules/fsevents/node_modules/os-homedir):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/os-homedir' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.os-homedir.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: os-tmpdir@1.0.2 (node_modules/chokidar/node_modules/fsevents/node_modules/os-tmpdir):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/os-tmpdir' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.os-tmpdir.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: path-is-absolute@1.0.1 (node_modules/chokidar/node_modules/fsevents/node_modules/path-is-absolute):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/path-is-absolute' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.path-is-absolute.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: process-nextick-args@2.0.1 (node_modules/chokidar/node_modules/fsevents/node_modules/process-nextick-args):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/process-nextick-args' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.process-nextick-args.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: safe-buffer@5.1.2 (node_modules/chokidar/node_modules/fsevents/node_modules/safe-buffer):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/safe-buffer' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.safe-buffer.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: safer-buffer@2.1.2 (node_modules/chokidar/node_modules/fsevents/node_modules/safer-buffer):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/safer-buffer' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.safer-buffer.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: sax@1.2.4 (node_modules/chokidar/node_modules/fsevents/node_modules/sax):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/sax' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.sax.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: semver@5.7.1 (node_modules/chokidar/node_modules/fsevents/node_modules/semver):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/semver' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.semver.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: set-blocking@2.0.0 (node_modules/chokidar/node_modules/fsevents/node_modules/set-blocking):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/set-blocking' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.set-blocking.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: signal-exit@3.0.2 (node_modules/chokidar/node_modules/fsevents/node_modules/signal-exit):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/signal-exit' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.signal-exit.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: strip-json-comments@2.0.1 (node_modules/chokidar/node_modules/fsevents/node_modules/strip-json-comments):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/strip-json-comments' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.strip-json-comments.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: util-deprecate@1.0.2 (node_modules/chokidar/node_modules/fsevents/node_modules/util-deprecate):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/util-deprecate' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.util-deprecate.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: wrappy@1.0.2 (node_modules/chokidar/node_modules/fsevents/node_modules/wrappy):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/wrappy' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.wrappy.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: yallist@3.1.1 (node_modules/chokidar/node_modules/fsevents/node_modules/yallist):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/yallist' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/chokidar/node_modules/fsevents/node_modules/.yallist.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: abbrev@1.1.1 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/abbrev):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/abbrev' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.abbrev.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: ansi-regex@2.1.1 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/ansi-regex):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/ansi-regex' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.ansi-regex.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: aproba@1.2.0 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/aproba):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/aproba' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.aproba.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: balanced-match@1.0.0 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/balanced-match):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/balanced-match' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.balanced-match.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: chownr@1.1.4 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/chownr):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/chownr' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.chownr.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: code-point-at@1.1.0 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/code-point-at):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/code-point-at' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.code-point-at.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: concat-map@0.0.1 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/concat-map):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/concat-map' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.concat-map.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: console-control-strings@1.1.0 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/console-control-strings):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/console-control-strings' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.console-control-strings.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: core-util-is@1.0.2 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/core-util-is):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/core-util-is' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.core-util-is.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: deep-extend@0.6.0 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/deep-extend):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/deep-extend' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.deep-extend.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: delegates@1.0.0 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/delegates):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/delegates' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.delegates.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: detect-libc@1.0.3 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/detect-libc):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/detect-libc' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.detect-libc.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fs.realpath@1.0.0 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/fs.realpath):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/fs.realpath' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.fs.realpath.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: has-unicode@2.0.1 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/has-unicode):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/has-unicode' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.has-unicode.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: inherits@2.0.4 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/inherits):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/inherits' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.inherits.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: ini@1.3.5 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/ini):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/ini' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.ini.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: isarray@1.0.0 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/isarray):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/isarray' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.isarray.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: minimist@1.2.5 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/minimist):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/minimist' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.minimist.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: ms@2.1.2 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/ms):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/ms' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.ms.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: npm-normalize-package-bin@1.0.1 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/npm-normalize-package-bin):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/npm-normalize-package-bin' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.npm-normalize-package-bin.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: number-is-nan@1.0.1 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/number-is-nan):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/number-is-nan' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.number-is-nan.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: object-assign@4.1.1 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/object-assign):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/object-assign' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.object-assign.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: os-homedir@1.0.2 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/os-homedir):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/os-homedir' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.os-homedir.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: os-tmpdir@1.0.2 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/os-tmpdir):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/os-tmpdir' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.os-tmpdir.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: path-is-absolute@1.0.1 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/path-is-absolute):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/path-is-absolute' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.path-is-absolute.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: process-nextick-args@2.0.1 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/process-nextick-args):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/process-nextick-args' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.process-nextick-args.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: safe-buffer@5.1.2 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/safe-buffer):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/safe-buffer' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.safe-buffer.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: safer-buffer@2.1.2 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/safer-buffer):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/safer-buffer' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.safer-buffer.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: sax@1.2.4 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/sax):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/sax' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.sax.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: semver@5.7.1 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/semver):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/semver' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.semver.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: set-blocking@2.0.0 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/set-blocking):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/set-blocking' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.set-blocking.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: signal-exit@3.0.2 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/signal-exit):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/signal-exit' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.signal-exit.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: strip-json-comments@2.0.1 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/strip-json-comments):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/strip-json-comments' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.strip-json-comments.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: util-deprecate@1.0.2 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/util-deprecate):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/util-deprecate' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.util-deprecate.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: wrappy@1.0.2 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/wrappy):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/wrappy' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.wrappy.DELETE'
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: yallist@3.1.1 (node_modules/jest-haste-map/node_modules/fsevents/node_modules/yallist):
npm WARN enoent SKIPPING OPTIONAL DEPENDENCY: ENOENT: no such file or directory, rename '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/yallist' -> '/data3/genome_graphs/sequenceTubeMap/node_modules/jest-haste-map/node_modules/fsevents/node_modules/.yallist.DELETE'

added 1568 packages from 935 contributors in 90.226s

33 packages are looking for funding
  run `npm fund` for details
```

It seems to have finished, and I don't know what kind of funding these packages are looking for... Well, apparently it's classical money, so nothing I'll worry about now.

```bash
npm run build
```

```
> sequence-tube-maps@0.1.0 build /data3/genome_graphs/sequenceTubeMap
> react-scripts build

Creating an optimized production build...
Compiled successfully.

File sizes after gzip:

  90.66 KB  build/static/js/2.d6337c92.chunk.js
  22.09 KB  build/static/css/2.f64b6d34.chunk.css
  17 KB     build/static/js/main.c7bbacc0.chunk.js
  2.5 KB    build/static/js/3.9125bfc7.chunk.js
  1.13 KB   build/static/js/runtime~main.06d9c9f7.js
  624 B     build/static/css/main.2c8b72f0.chunk.css

The project was built assuming it is hosted at the server root.
You can control this with the homepage field in your package.json.
For example, add this to build it for GitHub Pages:

  "homepage" : "http://myname.github.io/myapp",

The build folder is ready to be deployed.
You may serve it with a static server:

  yarn global add serve
  serve -s build

Find out more about deployment here:

  https://bit.ly/CRA-deploy
```

### Testing IVG locally

```bash
npm run serve
```

With the server running, I open a local console and use my standard ssh call, just with yet another localhost:

```bash
ssh -f -N -L 3000:localhost:3000 spo12@127.0.0.1 -p 1234
```

It works perfectly.


## MoMI-G

[MoMI-G](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-019-3145-2) is a new tools to visualise genome graphs - and it includes sequences tube maps and works with vg. The [documentation](https://momi-g.readthedocs.io/en/latest/index.html) is a little hard to follow at times, but I think this could be a very interesting tool to install and use on our server. It was built with the human genome in mind, so I'm not sure I will even be able to use it with my bacterial data.

